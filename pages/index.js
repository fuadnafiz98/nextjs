import Head from "next/head";
import styles from "../styles/Home.module.css";

export async function getStaticProps() {
  const res = await fetch("/api/posts");
  // console.log(res);
  const posts = await res.json();
  return {
    props: {
      posts,
    },
    revalidate: 1,
  };
}

export default function Home({ posts }) {
  return (
    <div>
      <Head>
        <title>page title</title>
      </Head>
      {posts.map(post => (
        <div key={post.postId}>
          <h2>{post.title}</h2>
          <h4>{post.body}</h4>
          <hr />
        </div>
      ))}
    </div>
  );
}
