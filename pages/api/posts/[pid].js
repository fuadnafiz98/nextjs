import { posts } from "./index";

export default (req, res) => {
  const {
    query: { pid },
  } = req;
  let post = posts.filter(post => post.postId == pid);
  console.log(post);
  res.statusCode = 200;
  res.json(JSON.stringify(post));
};
