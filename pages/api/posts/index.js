let posts = [
  {
    postId: 1,
    title: "first title",
    body: "first body",
  },
  {
    postId: 2,
    title: "first title",
    body: "first body",
  },
  {
    postId: 3,
    title: "first title",
    body: "first body",
  },
  {
    postId: 4,
    title: "first title",
    body: "first body",
  },
];
export { posts };
export default (req, res) => {
  if (req.method == "POST") {
    const post = req.body;
    post.postId = posts.length + 1;
    posts = [post, ...posts];
    res.statusCode = 200;
    res.json(JSON.stringify(posts));
  } else if (req.method == "GET") {
    res.statusCode = 200;
    res.json(JSON.stringify(posts));
  }
};
